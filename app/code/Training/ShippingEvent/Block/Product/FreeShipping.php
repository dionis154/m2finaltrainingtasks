<?php

namespace Training\ShippingEvent\Block\Product;

class FreeShipping extends \Magento\Catalog\Block\Product\View
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'sales/freeshipping/amount';

    public function canShow()
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT);
        $productPrice       = $this->getProduct()->getFinalPrice();
        return $productPrice >= $freeShippingAmount;
    }
}
