<?php

namespace Training\ShippingEvent\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session as CheckoutSession;
use Training\ShippingEvent\Model\EventFactory as ShippingEventFactory;

class Event extends \Magento\Framework\View\Element\Template
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'sales/freeShipping/amount';
    const XML_PATH_FREESHIPPING_ENABLE = 'sales/freeShipping/enable';

    protected $checkoutSession;

    protected $_eventFactory;

    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        array $data = [],
        ShippingEventFactory $eventFactory
    ) {
        $this->checkoutSession  = $checkoutSession;
        $this->_eventFactory    = $eventFactory;
        parent::__construct($context, $data);
    }

    public function getFreeShippingAmount()
    {
        $event = $this->_eventFactory->create();
        $event->setBaseTotalAmount("");
        return $this->_scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT);
    }

}
