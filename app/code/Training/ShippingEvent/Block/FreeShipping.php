<?php

namespace Training\ShippingEvent\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session as CheckoutSession;

class FreeShipping extends \Magento\Framework\View\Element\Template
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'sales/freeShipping/amount';
    const XML_PATH_FREESHIPPING_ENABLE = 'sales/freeShipping/enable';

    protected $checkoutSession;

    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        array $data = []
    ) {
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
    }

    public function getAmount()
    {
        $quote = $this->checkoutSession->getQuote();
        return $quote->getSubtotal();
    }

    public function getAmountLeft()
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT);
        $cartSubtotal       = $this->getAmount();
        return round($freeShippingAmount - $cartSubtotal, 2);
    }

    public function hasFreeShipping()
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT);
        $cartSubtotal       = $this->getAmount();
        return $cartSubtotal >= $freeShippingAmount;
    }

}
