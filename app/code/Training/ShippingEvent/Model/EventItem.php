<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\ShippingEvent\Model;


use Training\ShippingEvent\Api\Data\EventItemInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;


class EventItem extends AbstractModel implements EventItemInterface, IdentityInterface
{
    /**
     * CMS block cache tag
     */
    const CACHE_TAG = 'shipping_event_item';


    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'shipping_event_item';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\ShippingEvent\Model\ResourceModel\EventItem::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG];
    }

    /**
     * Retrieve Item id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ITEM_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return EventItemInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ITEM_ID, $id);
    }

    /**
     * Retrieve event id
     *
     * @return int
     */
    public function getEventId()
    {
        return $this->getData(self::EVENT_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return EventItemInterface
     */
    public function setEventId($id)
    {
        return $this->setData(self::EVENT_ID, $id);
    }

    /**
     * Retrieve product id
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Set Product ID
     *
     * @param int $id
     * @return EventItemInterface
     */
    public function setProductId($id)
    {
        return $this->setData(self::PRODUCT_ID, $id);
    }


}
