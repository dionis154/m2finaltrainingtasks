<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\ShippingEvent\Api\Data;

/**
 * CMS block interface.
 * @api
 * @since 100.0.2
 */
interface EventItemInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ITEM_ID       = 'item_id';
    const EVENT_ID      = 'event_id';
    const PRODUCT_ID    = 'product_id';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Event ID
     *
     * @return string|null
     */
    public function getEventId();

    /**
     * Get Product ID
     *
     * @return string|null
     */
    public function getProductId();

    /**
     * Set ID
     *
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Set Event ID
     *
     * @param $eventId
     * @return $this
     */
    public function setEventId($eventId);

    /**
     * Set Product ID
     *
     * @param $product
     * @return $this
     */
    public function setProductId($product);

}
