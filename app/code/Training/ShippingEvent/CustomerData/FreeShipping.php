<?php

namespace Training\ShippingEvent\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class FreeShipping extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'sales/freeshipping/amount';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $scopeConfig;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $freeShippingAmount = $this->scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT);
        $totals             = $this->getQuote()->getTotals();
        $subtotalAmount     = $totals['subtotal']->getValue();
        return [
            'has_freesipping' => ($subtotalAmount >= $freeShippingAmount),
            'amount_left' => $this->checkoutHelper->formatPrice($freeShippingAmount - $subtotalAmount)
        ];
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }
}
